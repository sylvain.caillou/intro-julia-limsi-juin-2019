module matrix
    use iso_c_binding, only: c_int32_t, c_double
    public :: mysum
contains
    subroutine mysum(m, n, x, s) bind( C, name="mysum" )
        implicit none
        integer(c_int32_t), intent(in) :: m, n
        real(c_double), dimension( 1:m, 1:n ), intent(in) :: x
        real(c_double), intent(out) :: s
        integer(c_int32_t) :: i, j
        s=0.0
        do i=1,m
            do j=1,n
                s=s+x(i,j)
            end do
        end do
    end subroutine mysum
end module matrix